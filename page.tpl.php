<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"> </script>
</head>

<body<?php print $onload_attributes ?>>

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td id="sitemast">
      <?php if ($logo) { ?><a id="logo" href="./" title="Home"><img src="<?php print $logo ?>" alt="Home" /></a><?php } ?>
      <?php if ($site_name) { ?><h1 class='site-name'><a href="./" title="Home"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
  </tr>
  <tr>
    <td id="menu">
      <?php if (isset($primary_links)) { ?><div id="primary"><?php print '<ul><li>' . theme('links', $primary_links, '</li><li>') . '</li></ul>' ?></div><?php } ?>
      <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print '<ul><li>' . theme('links', $secondary_links, '</li><li>') . '</li></ul>' ?></div><?php } ?>
      <?php print $search_box ?>
    </td>
  </tr>
<?php if ($header) { ?>
  <tr>
    <td id="subheader" colspan="2"><div><?php print $header ?></div></td>
  </tr>
<?php } ?>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>

        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<div id="footer">
  <p><?php print $footer_message ?></p>
</div>
<?php print $closure ?>
</body>
</html>
