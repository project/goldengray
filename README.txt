
Theme Name: Golden Gray
Description: golden gray for Drupal
Author: William Pramana
Contributor: Ported to Drupal by Chris Cook

Notes: This is a Drupal port of William Pramana's Golden Gray Wordpress theme. The
       markup and core styles are based on the bluemarine Drupal theme. It was a quick
       job and is not especially complete in its Drupal support so there are likely
       issues. It has been tested with Firefox 1.5, Opera 8, and IE 6 on Drupal 4.7.0-beta3
       with a modicum of modules (forum, blog, image, and other core modules) enabled.
       You can find William Pramana's original theme at http://wpram.com/.

Known issues include:
       * layout stretches with large images or other non-wrappable content
       * list bullets are Drupal default and not those of the original Golden Gray theme
       * site logo image is not supported
       * secondary links are not handled very well